#!/bin/bash
##   ModulOSC; An open platform for OSC control of analogue devices
##
##   Copyright (C) 2014  Doug Hammond
##
##   This program is free software: you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation, either version 3 of the License, or
##   (at your option) any later version.
##
##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.
##
##   You should have received a copy of the GNU General Public License
##   along with this program.  If not, see <http://www.gnu.org/licenses/>.


#
# ModulOSC service wrapper
#

INST_PATH=/home/root/build/arm

dbus-launch --sh-syntax 2>&1 > ${INST_PATH}/ModulOSC.dbus.sh
echo \$\* >> ${INST_PATH}/ModulOSC.dbus.sh
chmod +x ${INST_PATH}/ModulOSC.dbus.sh

LD_LIBRARY_PATH=${INST_PATH} ${INST_PATH}/ModulOSC.dbus.sh ${INST_PATH}/ModulOSC 2>&1 > ${INST_PATH}/ModulOSC.log &
